# Template Maker

## Como iniciar um projeto a partir daqui?

1. Token de desenvolvedor:
    - Lembre-se de ter usuários cadastrados como desenvolvedor na loja que você estiver trabalhando, tanto em staging quanto em produção.
2. Configuração do projeto:
    - Transfira o projeto para uma pasta que contenha a [build](https://gitlab.com/vnda/build) clonada. Certifique-se de ter as dependências da build instaladas.
3. Inicialização do repositório Git:
    - Crie um repositório no sistema de controle de versão da sua preferência.
    - No diretório do repositório execute os seguintes comandos:

```bash
git init
git remote add origin <url-do-repositorio>
git add .
git commit -m "Initial commit"
git push -u origin main # ou master
```

4. Instalação das dependências:
    Execute o seguinte comando para instalar as dependências do projeto:

```bash
npm install
```

5. Sincronização com a loja:
    Para sincronizar os arquivos do projeto com a loja, execute:

```bash
npm run sync
```

6. Desenvolvimento:
Para iniciar o ambiente de desenvolvimento e realizar modificações no projeto, utilize o comando:

```bash
npm run dev
```

7. Deploy:
Para fazer o deploy do projeto, execute:

```bash
npm run deploy
```

8. Envio das alterações ao repositório Git:
Após realizar as modificações e testes, suba as alterações para o repositório remoto:

```bash
git add .
git commit -m "Update project"
git push -u origin main # ou master
```
