const Header = {
  lastScrollTop: -1,
  scrollTop: window.scrollY,
  header: document.querySelector('#header'),

  setScroll: function (scrollTop, lastScrollTop) {
    const _this = this;

    if (scrollTop <= 0) {
      _this.header.classList.add('scroll-up');
      _this.header.classList.remove('scroll-down');
      _this.header.classList.add('on-top');
    } else {
      if (_this.header.classList.contains('on-top')) {
        _this.header.classList.remove('on-top');
      }
      if (scrollTop > lastScrollTop) {
        _this.header.classList.add('scroll-down');
        _this.header.classList.remove('scroll-up');
      } else {
        _this.header.classList.add('scroll-up');
        _this.header.classList.remove('scroll-down');
      }
    }
  },

  searchButton: function () {
    const searchButton = document.querySelector('[data-action="show-form-search"]');
    const searchButtonClose = document.querySelector('[data-action="hide-form-search"]');
    const formSearch = document.querySelector('.header-search');
    const formSearchInput = document.querySelector('.header-search input');

    if (!formSearch) return;

    searchButton &&
      searchButton.addEventListener('click', () => {
        formSearch.classList.toggle('-active', !formSearch.classList.contains('-active'));

        formSearchInput && formSearchInput.focus();

        if (window.innerWidth > 768) {
          document.addEventListener('scroll', () => {
            formSearch.classList.remove('-active');
          });
        }
      });

    searchButtonClose &&
      searchButtonClose.addEventListener('click', () => {
        formSearch.classList.remove('-active');
      });

    // Desativa o popup de busca - quando necessário
    formSearch.addEventListener('click', (e) => {
      if (e.target.className == 'header-search -active') {
        formSearch.classList.remove('-active');
      }
    });
  },

  init: function () {
    const _this = this;

    _this.setScroll(_this.scrollTop, _this.lastScrollTop);
    _this.searchButton();

    // Atualiza o header quando a page recebe scroll
    window.addEventListener('scroll', function () {
      const newSt = window.scrollY;

      _this.setScroll(newSt, _this.lastScrollTop);
      _this.lastScrollTop = newSt;
    });
  },
};

export default Header;
