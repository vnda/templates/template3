import LazyLoading from './common/lazyLoading';
import LoadPurchase from './common/loadPurchase.js';
import CartDrawer from './common/cartDrawer';
import Footer from './common/footer';
import Header from './common/header';
import Webforms from './common/webforms';
import { NewsletterComponent, PriceComponent } from './components/vndaComponents.js';
import { setSearch } from './components/utilities';

// ==========================================
// Inicialização
// ==========================================
console.log(
  '%cOlist - Soluções de vendas online e Serviços de E-commerce',
  'color: #0a4ee4; font-size: 14px; font-family: "Verdana", sans-serif; font-weight: bold;'
);

window.addEventListener('DOMContentLoaded', () => {
  Header.init();
  LazyLoading.init();
  LoadPurchase.init();
  PriceComponent.init();
  CartDrawer.init();
  Footer.init();
  Webforms.init();
  NewsletterComponent.init();
  setSearch();
});
