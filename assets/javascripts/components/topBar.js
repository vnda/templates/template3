import Swiper from 'swiper';
import { Autoplay } from 'swiper/modules';

export default function setTopBar() {
  const topBar = document.querySelector("[data-top-bar]");
  if (!topBar) return

  const topBarSwiper = new Swiper(topBar.querySelector('.swiper'), {
    modules: [Autoplay],
    slidesPerView: 1,
    spaceBetween: 0,
    watchOverflow: true,
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
      pauseOnMouseEnter: true,
    },
  });

  // Autoplay
  topBarSwiper.autoplay.stop();
  const eventType = window.mobile ? 'scroll' : 'mousemove';
  window.addEventListener(eventType, topBarSwiper.autoplay.start, { once: true });
}