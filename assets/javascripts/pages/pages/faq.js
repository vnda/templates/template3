import { slideDown, slideToggle } from '../../components/utilities';

export default function accordionsFaq() {
  var collapseWrapper;
  var contentWrapper;
  var content;
  var collapses = document.querySelectorAll('[data-collapse]');
  var timeout = null;
  var firstOpen = false;

  // No primeiro load verifica se tem o id do collapse na url e abre ela

  let urlCollapseId = document.location.hash;

  if (urlCollapseId && collapses.length > 0) {
    collapseWrapper = document.querySelector(urlCollapseId);
    contentWrapper = collapseWrapper.querySelector('.content-wrapper');
    content = collapseWrapper.querySelector('.content');

    collapseWrapper.classList.add('-open');

    // Da scroll na página até o collapse
    window.scrollTo({
      top: contentWrapper.offsetTop - 300,
      behavior: 'smooth',
    });

    // Abre o collapse
    slideDown(contentWrapper, content, 300);
  } else if (collapses.length > 0) {
    timeout = setTimeout(() => {
      var firstBtn = document.querySelectorAll('.content-wrapper')[0];
      var firstcontent = firstBtn.querySelector('.content');
      collapses[0].classList.add('-open');

      slideDown(firstBtn, firstcontent, 0);
      firstOpen = true;
    }, 3000);
  }

  // Lida com o clique nos collapses
  var collapseButtons = document.querySelectorAll('[open-collapse]');

  collapseButtons.forEach((button) => {
    button.addEventListener('click', (e) => {
      e.preventDefault();

      if (!firstOpen && timeout != null) {
        clearTimeout(timeout);
        firstOpen = true;
      }

      collapseWrapper = button.closest('[data-collapse]');
      contentWrapper = collapseWrapper.querySelector('.content-wrapper');
      content = collapseWrapper.querySelector('.content');

      collapseWrapper.classList.toggle('-open');

      slideToggle(contentWrapper, content, 300);
    });
  });
}
