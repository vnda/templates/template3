export default function setTabs() {
  let tabs = document.querySelectorAll('[data-tab]');

  if (tabs.length > 0) {
    tabs.forEach((tab) => {
      tab.addEventListener('click', (event) => {
        event.preventDefault();
        let activeTab = document.querySelector('.tab-btn.-active');
        let activeContet = document.querySelector('.content.-active');

        if (activeTab) {
          activeTab.classList.remove('-active');
        }

        if (activeContet) {
          activeContet.classList.remove('-active');
        }

        let tabId = tab.getAttribute('href');
        let tabTarget = document.querySelector('a[href="' + tabId + '"]');
        let contentTarget = document.querySelector(tabId);

        if (tabTarget) {
          tabTarget.classList.add('-active');
          contentTarget.classList.add('-active');

          setTimeout(() => {
            window.scrollTo({ top: 0, behavior: 'smooth' });
          }, 100);

          if (window.innerWidth < 992) {
            const sidePositioning = tab.offsetLeft - window.innerWidth / 2 + tab.offsetWidth / 2;
            tab.offsetParent.scrollTo({ left: sidePositioning, behavior: 'smooth' });
          }
        }
      });
    });

    const handleTabsByHash = () => {
      let id = document.location.hash;

      if (id) {
        let tabTarget = document.querySelector('a[href="' + id + '"]');

        if (tabTarget) {
          tabTarget.click();
        }
      }
    };

    handleTabsByHash();
    window.addEventListener('hashchange', handleTabsByHash);
  }
}
