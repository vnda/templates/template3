import setFullbanner from './home/1_fullbanner';
import setProductSlider from './home/3_product_slider';
import CategoryTabs from './home/3_category_tabs';
import setBrandsCarousel from './home/brands_carousel';
import setQuotationVerticalSwiper from './home/3_quotation';
import setBlogHighlightsSwiper from './home/3_categories';

//addImports

import setTopBar from '../components/topBar';
import handleConditionalLazy from '../components/conditionalLazy';

const Home = {
  init: function () {
    setTopBar();
    handleConditionalLazy();

    setFullbanner()
		setProductSlider()
		CategoryTabs.init();
		setBrandsCarousel();
		setQuotationVerticalSwiper();
		setBlogHighlightsSwiper();
		
		//calls
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Home.init();
});
