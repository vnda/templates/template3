import Swiper from 'swiper';
import { Pagination } from 'swiper/modules';

export default function setProductSlider() {
  const sections = document.querySelectorAll('[data-products-slider]');

  sections.forEach((section) => {
    const swiperEl = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');

    if (swiperEl) {
      const carousel = new Swiper(swiperEl, {
        modules: [Pagination],
        slidesPerView: 2,
        spaceBetween: 8,
        watchOverflow: true,
        speed: 1000,
        pagination: {
          el: pagination,
          type: 'bullets',
          clickable: true,
        },
        breakpoints: {
          768: {
            slidesPerView: 3,
            spaceBetween: 24,
          },
          1440: {
            slidesPerView: 4,
            spaceBetween: 24,
          },
        },
      });
    }
  });
}
