import Swiper from 'swiper';
import { Pagination } from 'swiper/modules';

export default function setBlogHighlightsSwiper() {
  const sections = document.querySelectorAll('[data-blog-highlights]');

  sections.forEach((section) => {
    const swiperEl = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');

    if (swiperEl) {
      const carousel = new Swiper(swiperEl, {
        modules: [Pagination],
        slidesPerView: 1.1,
        spaceBetween: 8,
        speed: 1000,
        watchOverflow: true,
        pagination: {
          el: pagination,
          type: 'bullets',
          clickable: true,
        },
        breakpoints: {
          768: {
            slidesPerView: 2,
            spaceBetween: 24,
          },
          1280: {
            slidesPerView: 3,
            spaceBetween: 24,
          },
        },
      });
    }
  });
}
