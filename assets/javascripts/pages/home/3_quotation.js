import Swiper from 'swiper';
import { Navigation, Pagination } from 'swiper/modules';

export default function setQuotationVerticalSwiper() {
  const sections = document.querySelectorAll('[data-quotation-vertical]');

  sections.forEach((section) => {
    const swiperEl = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');
    const next = section.querySelector('.swiper-button-next');
    const prev = section.querySelector('.swiper-button-prev');

    if (swiperEl) {
      const carousel = new Swiper(swiperEl, {
        modules: [Navigation, Pagination],
        slidesPerView: 1,
        spaceBetween: 10,
        speed: 1000,
        watchOverflow: true,
        pagination: {
          el: pagination,
          type: 'bullets',
          clickable: true,
        },
        breakpoints: {
          991: {
            navigation: {
              nextEl: next,
              prevEl: prev,
            },
          },
        },
      });
    }
  });
}
