import Swiper from 'swiper';
import { Manipulation, Navigation, Pagination, Zoom, Thumbs } from 'swiper/modules';

export default function setGallery(Product) {
  const thumbs = new Swiper(Product.thumbsSlider.element, {
    modules: [Manipulation],
    direction: 'vertical',
    slidesPerView: 4,
    slidesPerGroup: 1,
    spaceBetween: 10,
    watchSlidesProgress: true,
    watchOverflow: true,
    preloadImages: false,
  });

  const main = new Swiper(Product.mainSlider.element, {
    modules: [Manipulation, Navigation, Pagination, Zoom, Thumbs],
    slidesPerView: 1,
    slidesPerGroup: 1,
    speed: 1000,
    watchSlidesProgress: true,
    watchOverflow: true,
    preloadImages: false,
    navigation: {
      nextEl: '[data-main-slider] .swiper-button-next',
      prevEl: '[data-main-slider] .swiper-button-prev',
    },
    pagination: {
      el: '[data-product-images] .swiper-pagination',
      clickable: true,
      type: 'bullets',
    },
    breakpoints: {
      992: {
        direction: 'vertical',
        slidesPerView: 1.2,
        spaceBetween: 8,
      },
    },
    zoom: {
      maxRatio: 1.5,
      zoomedSlideClass: '-zoomed',
    },
  });

  Product.thumbsSlider.swiper = thumbs;
  Product.mainSlider.swiper = main;
}
