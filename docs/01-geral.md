- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/01-geral.png)

# GERAL

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

## POPUP DE NEWSLETTER

**_Observações:_**

Criar formulário no [Admin](https://template3.vnda.dev/admin/config/mensagens-e-avisos/forms) com o assunto `newsletter`. Serve para o popup e a newsletter do rodapé.

&nbsp;

**_Informações:_**

| Dúvida                | Instrução                                              |
| --------------------- | ------------------------------------------------------ |
| **Onde cadastrar**    | Banners                                                |
| **Onde será exibido** | Popup com newsletter que aparece uma vez a cada 7 dias |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/)               |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                       |
| ------------- | ------------------- | -------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 500x500 pixels |
| **Título**    | :black_circle:      | Controle interno                 |
| **Subtítulo** | :large_blue_circle: | Título da newsletter             |
| **Descrição** | :large_blue_circle: | Texto da newsletter              |
| **Externo?**  | :no_entry:          |                                  |
| **URL**       | :no_entry:          |                                  |
| **Posição**   | :black_circle:      | `newsletter-popup`               |
| **Cor**       | :no_entry:          |                                  |

**Obs.:**

Caso deseje trocar o texto após o envio do formulário, separar os dois textos com `---` no campo descrição. O segundo texto (abaixo da divisão) será exibido somente se o formulário de newsletter for enviado corretamente.

É possível adicionar no máximo 2 parágrafos para o texto descritivo abaixo do título. Conforme modelo abaixo.

&nbsp;

**_Modelo para Descrição_**

```md
Se inscreva na nossa newsletter para receber em primeira mão as últimas notícias, promoções exclusivas e dicas incríveis. Não perca nada!

Descrição extra lorem ipsumt sit dolor amet

---

Enviado com sucesso!
```

&nbsp;

## CARRINHO - FRETE GRÁTIS

**_Informações:_**

| Dúvida                | Instrução                                |
| --------------------- | ---------------------------------------- |
| **Onde cadastrar**    | Banners                                  |
| **Onde será exibido** | Frete grátis                             |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                            |
| ------------- | ------------------- | ------------------------------------- |
| **Imagem**    | :no_entry:          |                                       |
| **Título**    | :black_circle:      | Não exibido no front                  |
| **Subtítulo** | :large_blue_circle: | Preço para o frete grátis. Ex.: `500` |
| **Descrição** | :no_entry:          |                                       |
| **Externo?**  | :no_entry:          |                                       |
| **URL**       | :no_entry:          |                                       |
| **Posição**   | :black_circle:      | `frete-gratis`                        |
| **Cor**       | :no_entry:          |                                       |

&nbsp;

## SUGESTÕES CARRINHO

**_Informações:_**

| Dúvida                          | Instrução                                                                       |
| ------------------------------- | ------------------------------------------------------------------------------- |
| **Onde cadastrar**              | Tags                                                                            |
| **Onde será exibido**           | Sugestões de produtos no carrinho                                               |
| **Cadastro exemplo em staging** | [Admin](https://template3.vnda.dev/admin/tags/editar?id=sugestoes-carrinho) |

&nbsp;

**_Informações sobre os campos_**

| Campo         | Funcional?          | Orientação                                                                             |
| ------------- | ------------------- | -------------------------------------------------------------------------------------- |
| **Nome**      | :black_circle:      | `sugestoes-carrinho`                                                                   |
| **Título**    | :large_blue_circle: | Título da seção no carrinho. Se não for cadastrado título, o padrão é `Compre também:` |
| **Subtítulo** | :no_entry:          |                                                                                        |
| **Descrição** | :no_entry:          |                                                                                        |
| **Tipo**      | :no_entry:          |                                                                                        |
| **Imagem**    | :no_entry:          |                                                                                        |

&nbsp;

## BANNER FAIXA TOPO

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Faixa exibida acima do header                                 |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação            |
| ------------- | ------------------- | --------------------- |
| **Imagem**    | :no_entry:          |                       |
| **Título**    | :black_circle:      | Utilizado para controle interno |
| **Subtítulo** | :no_entry:          |                       |
| **Descrição** | :large_blue_circle: | Texto que será exibido na faixa |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba        |
| **URL**       | :large_blue_circle: | Link de direcionamento                                        |
| **Posição**   | :black_circle:      | `geral-faixa-topo`    |
| **Cor**       | :large_blue_circle: | `Cor do fundo \| Cor do texto`. Ex.: `#000000 \| #FFFFFF` |

**Importante**

Para adicionar mais faixas, basta duplicar os banners e alterar os conteúdos conforme desejar

&nbsp;


&nbsp;

### HEADER

## LOGO PRINCIPAL

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Logo principal no header                                      |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação            |
| ------------- | ------------------- | --------------------- |
| **Imagem**    | :no_entry:          | Dimensões recomendadas: 200x40px              |
| **Título**    | :black_circle:      | Controle interno      |
| **Subtítulo** | :no_entry:          |                       |
| **Descrição** | :no_entry:          |                       |
| **Externo?**  | :no_entry:          |                       |
| **URL**       | :no_entry:          |                       |
| **Posição**   | :black_circle:      | `header-logo`         |
| **Cor**       | :no_entry:          |                       |

&nbsp;

## MENU PRINCIPAL

**_Informações:_**

| Dúvida                | Instrução                                           |
| --------------------- | --------------------------------------------------- |
| **Onde cadastrar**    | Menu                                                |
| **Onde será exibido** | Menus principal do site                             |
| **Níveis**            | 3 níveis                                            |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/admin/navegacao) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                                     |
| ------------- | ------------------- | -------------------------------------------------------------- |
| **Título**    | :black_circle:      | Título do menu                                                 |
| **Tooltip**   | :black_circle:      | Cor do texto do menu                                           |
| **Descrição** | :no_entry:          |                                                                |
| **Posição**   | :black_circle:      | `principal`                                                    |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do menu deve abrir em outra aba           |
| **URL**       | :black_circle:      | Link de direcionamento, se for `#` o menu não vai ser clicável |

&nbsp;

## BANNER DO SUBMENU

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Banners no submenu do header                                  |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                              |
| ------------- | ------------------- | --------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensões sugeridas de 620x420 pixels   |
| **Título**    | :black_circle:      | Alt da imagem                           |
| **Subtítulo** | :no_entry:          |                                         |
| **Descrição** | :no_entry:          |                                         |
| **Externo?**  | :no_entry:          |                                         |
| **URL**       | :no_entry:          |                                         |
| **Posição**   | :black_circle:      | `banner-menu-` + título do submenu. Ex.: `banner-menu-novidades` |
| **Cor**       | :no_entry:          |                                         |
### RODAPÉ

## NEWSLETTER

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Texto do bloco de newsletter do rodapé                        |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação           |
| ------------- | ------------------- | -------------------- |
| **Imagem**    | :large_blue_circle: |                      |
| **Título**    | :black_circle:      | Não exibido no front |
| **Subtítulo** | :large_blue_circle: | Título da Newsletter |
| **Descrição** | :large_blue_circle: | Breve descrição      |
| **Externo?**  | :no_entry:          |                      |
| **URL**       | :no_entry:          |                      |
| **Posição**   | :black_circle:      | `rodape-newsletter`  |
| **Cor**       | :no_entry:          |                      |

&nbsp;

## RODAPÉ LOGO

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Logo do rodapé.                                               |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação           |
| ------------- | ------------------- | -------------------- |
| **Imagem**    | :large_blue_circle: | Dimensões sugeridas de 320px de largura e altura livre |
| **Título**    | :black_circle:      | Não exibido no front |
| **Subtítulo** | :no_entry:          |                      |
| **Descrição** | :no_entry:          |                      |
| **Externo?**  | :no_entry:          |                      |
| **URL**       | :no_entry:          |                      |
| **Posição**   | :black_circle:      | `rodape-logo`        |
| **Cor**       | :no_entry:          |                      |

&nbsp;

## RODAPÉ ATENDIMENTO

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Coluna de atendimento do rodapé.                              |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação           |
| ------------- | ------------------- | -------------------- |
| **Imagem**    | :large_blue_circle: | 50x50px              |
| **Título**    | :black_circle:      | Não exibido no front |
| **Subtítulo** | :large_blue_circle: | Texto do ítem de atendimento |
| **Descrição** | :no_entry:          |                      |
| **Externo?**  | :large_blue_circle: | Marcar se o link deve abrir uma nova guia |
| **URL**       | :large_blue_circle: | Link para o redirecionamento |
| **Posição**   | :black_circle:      | `rodape-atendimento` |
| **Cor**       | :no_entry:          |                      |

**Orientações sobre as URLs**

**_Redirecionamento para Whatsapp_**

Para redirecionar ao whatsapp, já com uma mensagem preparada, o link abaixo pode ser utilizado e modificado para atender essa questão.

- O termo `phone` deve receber 55 referente ao país, o DDD e o número do telefone em si. Ex.: "5551999999999";
- Em `text`, inserir a mensagem desejada. É possível inserir alguma URL, acentuação etc... Para adicionar quebra de linhas, utilizar o termo `%0a`;
- Editar o modelo abaixo como desejar:

```md

https://api.whatsapp.com/send?phone=5551999999999&text=Olá, estou entrando em contato!%0a%0ahttps://endovita.vnda.dev

```

&nbsp;

**_Redirecionar para o app de e-mail do cliente_**
Para utilizar o aplicativo de envios de e-mail do usuário, inserir no campo `url` o termo `mailto:` + e-mail destinatário da mensagem. Ex.: `mailto:contato@loja.com.br`

&nbsp;

## REDES SOCIAIS

**_Informações:_**

| Dúvida                | Instrução                                           |
| --------------------- | --------------------------------------------------- |
| **Onde cadastrar**    | Menu                                                |
| **Onde será exibido** | Links de redes sociais do rodapé                    |
| **Níveis**            | 1 nível                                             |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/admin/navegacao) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                           |
| ------------- | ------------------- | ---------------------------------------------------- |
| **Título**    | :black_circle:      | Título do menu                                       |
| **Tooltip**   | :black_circle:      | nome do icone                                        |
| **Descrição** | :no_entry:          |                                                      |
| **Posição**   | :black_circle:      | `social`                                             |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do menu deve abrir em outra aba |
| **URL**       | :black_circle:      | Link de direcionamento                               |

&nbsp;

**_Opções de redes sociais_**

- facebook
- instagram
- youtube
- whatsapp
- x (novo logo - inserir "x")
- twitter (antigo logo - inserir "twitter")
- pinterest
- linkedin
- tiktok

&nbsp;

## MENU FOOTER

**_Informações:_**

| Dúvida                | Instrução                                           |
| --------------------- | --------------------------------------------------- |
| **Onde cadastrar**    | Menu                                                |
| **Onde será exibido** | Menu do rodapé                                      |
| **Níveis**            | 2 níveis                                            |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/admin/navegacao) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                           |
| ------------- | ------------------- | ---------------------------------------------------- |
| **Título**    | :black_circle:      | Título do menu                                       |
| **Tooltip**   | :no_entry:          |                                                      |
| **Descrição** | :no_entry:          |                                                      |
| **Posição**   | :black_circle:      | `rodape`                                             |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do menu deve abrir em outra aba |
| **URL**       | :black_circle:      | Link de direcionamento                               |

&nbsp;

## ASSINATURA - CNPJ

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Cnpj da loja no fim do rodapé                                 |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                    |
| ------------- | ------------------- | --------------------------------------------- |
| **Imagem**    | :no_entry:          |                                               |
| **Título**    | :black_circle:      | Utilizado para controle interno               |
| **Subtítulo** | :large_blue_circle: | Texto com informações conforme exemplo abaixo |
| **Descrição** | :no_entry:          |                                               |
| **Externo?**  | :no_entry:          |                                               |
| **URL**       | :no_entry:          |                                               |
| **Posição**   | :black_circle:      | `geral-infos-loja`                            |
| **Cor**       | :no_entry:          |                                               |

**_Observações_**

Exemplo de texto:

```md
© Nome da loja – CNPJ 00.000.000/0000-00
```
***
