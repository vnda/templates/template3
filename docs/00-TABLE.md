
<!-- _class: table-of-contents -->

# Orientações de cadastro

![Logo Vnda](../images/prints/vnda.svg)

## [Tabela de conteúdos](#1)

- ### [GERAL](#2)    - [1 POPUP DE NEWSLETTER](#2)    - [2 CARRINHO - FRETE GRÁTIS](#2)    - [3 SUGESTÕES CARRINHO](#2)    - [4 BANNER FAIXA TOPO](#2)    - [5 LOGO PRINCIPAL](#2)    - [6 MENU PRINCIPAL](#2)    - [7 BANNER DO SUBMENU](#2)    - [8 NEWSLETTER](#2)    - [9 RODAPÉ LOGO](#2)    - [10 RODAPÉ ATENDIMENTO](#2)    - [11 REDES SOCIAIS](#2)    - [12 MENU FOOTER](#2)    - [13 ASSINATURA - CNPJ](#2) - ### [HOME](#3)    - [1 FULLBANNER PRINCIPAL](#3)    - [2 BANNERS MOSAICO](#3)    - [3 BANNERS MOSAICO - TITULO](#3)    - [4 SLIDER DE PRODUTOS COM IMAGEM](#3)    - [5 TABS DE CATEGORIAS](#3)    - [6 TITULO TABS DE CATEGORIAS](#3)    - [7 MARCAS - CARROSSEL](#3)    - [8 INSTAGRAM - TÍTULO E BOTÃO](#3) - ### [TAG](#4)    - [1 TAG FULLBANNER](#4)    - [2 SLIDER DE CATEGORIAS](#4)    - [3 FILTRO](#4)    - [4 TAG FLAG](#4) - ### [PRODUTO](#5)    - [1 IMAGENS](#5)    - [2 DESCRIÇÃO](#5)    - [3 VARIANTES](#5)    - [4 TAG MODELO](#5)    - [5 GUIA DE MEDIDAS](#5)    - [6 TAG BANNER](#5)    - [7 BANNERS DE PRODUTO](#5)    - [8 SEÇÃO COMPRE JUNTO (até 3 produtos)](#5)    - [9 PRODUTOS RELACIONADOS](#5) - ### [ATENDIMENTO](#6)    - [1 FORMULÁRIO DE CONTATO](#6)    - [2 TEXTO FORMULÁRIO](#6)    - [3 BANNER DE INFORMAÇÕES DA LOJA (até 4)](#6)    - [4 SEÇÃO DE ABAS](#6)    - [5 SEÇÃO DE ABAS - TÍTULO](#6) - ### [SOBRE](#7)    - [1 FULLBANNER TOPO](#7)    - [2 BANNER CONTEÚDO - TEXTO SUPERIOR](#7)    - [3 BANNER IMAGEM E TEXTO](#7)    - [4 BANNER HORIZONTAL](#7) - ### [ONDE-ENCONTRAR](#8)    - [1 FULLBANNER TOPO](#8)    - [2 LOCAIS](#8) - ### [TERMOS](#9)    - [1 ABAS LATERAIS](#9) 

***
