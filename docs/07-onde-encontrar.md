
- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/07-onde-encontrar.png)

# ONDE-ENCONTRAR

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

**_Informações:_**

| Dúvida                       | Instrução                                                             |
| ---------------------------- | --------------------------------------------------------------------- |
| **Onde cadastrar**           | Páginas                                                               |
| **Onde será exibido**        | Página de onde-encontrar da loja                                        |
| **Cadastro exemplo**         | [Admin](https://template3.vnda.dev/admin/paginas/editar?id=onde-encontrar) |
| **Página para visualização** | [Página](https://template3.vnda.dev/p/onde-encontrar)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?     | Orientação                                          |
| ------------- | -------------- | --------------------------------------------------- |
| **Titulo**    | :black_circle: | Titulo da página                                    |
| **Url**       | :black_circle: | "onde-encontrar"                                      |
| **Descrição** | :black_circle: | Descrição da meta tag. Utilizada para melhorar SEO. |
| **Descrição** | :black_circle: | "."                                                 |

&nbsp;

## FULLBANNER TOPO

**_Informações:_**

| Dúvida                | Instrução                                        |
| --------------------- | ------------------------------------------------ |
| **Onde cadastrar**    | Banners                                          |
| **Onde será exibido** | Topo do layout padrão das páginas institucionais |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/)         |

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                          |
| ------------------- | ------------------- | ------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x364 pixels, Mobile: 1000x483 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                       |
| **Subtítulo**       | :large_blue_circle: | `Texto do botão \| Posição do texto`. Opções abaixo                   |
| **Descrição**       | :large_blue_circle: | Texto do banner em markdown. Exemplo abaixo                         |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba              |
| **URL**             | :large_blue_circle: | Link de direcionamento                                              |
| **Posição Desktop** | :black_circle:      | `onde-encontrar-banner-principal`                              |
| **Posição Mobile**  | :black_circle:      | `onde-encontrar-banner-principal-mobile`                       |
| **Cor**             | :large_blue_circle: | Cor do texto                                                        |

&nbsp;

**_Posições disponíveis:_**

- left
- center
- right

left: alinhado à esquerda
center: centralizado
right: alinhado à direita

**_Importante_**

Por padrão, a posição do texto é `center`. Para alterar a posição, sem utilizar botão, basta inserir `\| posição desejada`. Ex.: `\| left`

**_Exemplo Descrição do Banner:_**

```md
\#\#\# Upper title

\# Título do Banner

Breve descrição do banner.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

## LOCAIS

- Onde cadastrar? [Admin de locais](https://template3.vnda.dev/admin/config/gerais/locais)
- É necessário marcar a opção "Listar local no ecommerce" no cadastro do local.

**Campos que aparecem no front no cadatro dos locais**

| Campo                         | Funcional?     | Orientação                   |
| ----------------------------- | -------------- | ---------------------------- |
| **Listar local no ecommerce** | :black_circle: | Marcar para exibir na página |
| **Nome**                      | :black_circle: | Nome do local                |
| **Linha 1 do endereço**       | :black_circle: | Endereço principal do local  |
| **Cidade**                    | :black_circle: | Cidade do local              |
| **Bairro**                    | :black_circle: | Bairro do local              |
| **Estado**                    | :black_circle: | Estado do local              |
| **Cep**                       | :black_circle: | Cep do local                 |
| **Email**                     | :black_circle: | Email do local               |
| **Telefone principal**        | :black_circle: | Telefone principal do local  |
| **Latitude**                  | :black_circle: | Latitude do local            |
| **Longitude**                 | :black_circle: | Longitude do local           |
| **Imagens**                   | :black_circle: | Foto do local - Dimensões sugeridas de 480x220 pixels |

&nbsp;
***
