
- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/08-termos.png)

# TERMOS

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

**_Informações:_**

| Dúvida                       | Instrução                                                             |
| ---------------------------- | --------------------------------------------------------------------- |
| **Onde cadastrar**           | Páginas                                                               |
| **Onde será exibido**        | Página de termos da loja                                        |
| **Cadastro exemplo**         | [Admin](https://template3.vnda.dev/admin/paginas/editar?id=termos) |
| **Página para visualização** | [Página](https://template3.vnda.dev/p/termos)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?     | Orientação                                          |
| ------------- | -------------- | --------------------------------------------------- |
| **Titulo**    | :black_circle: | Titulo da página                                    |
| **Url**       | :black_circle: | "termos"                                      |
| **Descrição** | :black_circle: | Descrição da meta tag. Utilizada para melhorar SEO. |
| **Descrição** | :black_circle: | "."                                                 |

&nbsp;

## ABAS LATERAIS

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Seção de abas laterais                                        |
| **Cadastro exemplo**  | [Admin](https://template3.vnda.dev/)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                          |
| ------------- | ------------------- | ----------------------------------- |
| **Imagem**    | :no_entry:          |                                     |
| **Título**    | :black_circle:      | Título do banner. Apenas para controle interno                       |
| **Subtítulo** | :black_circle:      | Título da aba                       |
| **Descrição** | :black_circle:      | Conteúdo da aba. Aceita Markdown    |
| **Externo?**  | :no_entry:          |                                     |
| **URL**       | :large_blue_circle: | Id da aba. Ex.: "termos-de-uso", "politicas-dado". O id servirá para vincular o botão ao conteúdo textual e também como link para menus em rodapé etc |
| **Posição**   | :black_circle:      | `termos-abas`                 |
| **Cor**       | :no_entry:          |                                     |

***
