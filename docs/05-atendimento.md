
- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/05-atendimento.png)

# ATENDIMENTO

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

**_Informações:_**

| Dúvida                       | Instrução                                                             |
| ---------------------------- | --------------------------------------------------------------------- |
| **Onde cadastrar**           | Páginas                                                               |
| **Onde será exibido**        | Página de atendimento da loja                                        |
| **Cadastro exemplo**         | [Admin](https://template3.vnda.dev/admin/paginas/editar?id=atendimento) |
| **Página para visualização** | [Página](https://template3.vnda.dev/p/atendimento)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?     | Orientação                                          |
| ------------- | -------------- | --------------------------------------------------- |
| **Titulo**    | :black_circle: | Titulo da página                                    |
| **Url**       | :black_circle: | "atendimento"                                      |
| **Descrição** | :black_circle: | Descrição da meta tag. Utilizada para melhorar SEO. |
| **Descrição** | :black_circle: | "."                                                 |

&nbsp;


## FORMULÁRIO DE CONTATO

- Criar formulário no [Admin](https://template3.vnda.dev/admin/config/mensagens-e-avisos/forms).
- Com o assunto `contato`.

&nbsp;

## TEXTO FORMULÁRIO

**_Informações:_**

| Dúvida                          | Instrução                                                |
| ------------------------------- | -------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                  |
| **Onde será exibido**           | Acima do formulário de contato                           |
| **Cadastro exemplo em staging** | [Admin](https://template3.vnda.dev/) |

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação               |
| ------------- | ------------------- | ------------------------ |
| **Imagem**    | :no_entry:          |                          |
| **Título**    | :black_circle:      | Controle interno         |
| **Subtítulo** | :large_blue_circle: | Título do formulário     |
| **Descrição** | :large_blue_circle: | Texto do formulário      |
| **Externo?**  | :no_entry:          |                          |
| **URL**       | :no_entry:          |                          |
| **Posição**   | :black_circle:      | `atendimento-form-texto` |
| **Cor**       | :no_entry:          |                          |

&nbsp;

## BANNER DE INFORMAÇÕES DA LOJA (até 4)

**_Informações:_**

| Dúvida                          | Instrução                                                |
| ------------------------------- | -------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                  |
| **Onde será exibido**           | Abaixo do texto do formulário                            |
| **Cadastro exemplo em staging** | [Admin](https://template3.vnda.dev/) |

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                     |
| ------------- | ------------------- | ------------------------------ |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 24x24 pixels |
| **Título**    | :black_circle:      | Alt da imagem                  |
| **Subtítulo** | :no_entry:          |                                |
| **Descrição** | :large_blue_circle: | Texto curto da seção           |
| **Externo?**  | :no_entry:          |                                |
| **URL**       | :no_entry:          |                                |
| **Posição**   | :black_circle:      | `atendimento-infos-loja`       |
| **Cor**       | :no_entry:          |                                |

&nbsp;

## SEÇÃO DE ABAS

***Informações:***

| Dúvida                          | Instrução                                                                                                                |
| ------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| **Onde cadastrar**              | Banners                                                                                                                  |
| **Onde será exibido**           | Abas de perguntas frequentes                             |
| **Cadastro exemplo em staging** | [Admin](https://template3.vnda.dev/)                                                                                     |

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação               |
| ------------- | ------------------- | ------------------------ |
| **Imagem**    | :no_entry:          |                          |
| **Título**    | :black_circle:      | Controle interno         |
| **Subtítulo** | :large_blue_circle: | Título da aba            |
| **Descrição** | :large_blue_circle: | Texto da aba em Markdown |
| **Externo?**  | :no_entry:          |                          |
| **URL**       | :large_blue_circle: | ID único da aba          |
| **Posição**   | :black_circle:      | `atendimento-faq-item`      |
| **Cor**       | :no_entry:          |                          |

&nbsp;

## SEÇÃO DE ABAS - TÍTULO

**_Informações:_**

| Dúvida                          | Instrução                                                |
| ------------------------------- | -------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                  |
| **Onde será exibido**           | Título das abas de perguntas frequentes                  |
| **Cadastro exemplo em staging** | [Admin](https://template3.vnda.dev/) |

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação               |
| ------------- | ------------------- | ------------------------ |
| **Imagem**    | :no_entry:          |                          |
| **Título**    | :black_circle:      | Controle interno         |
| **Subtítulo** | :large_blue_circle: | Título da seção          |
| **Descrição** | :no_entry:          |                          |
| **Externo?**  | :no_entry:          |                          |
| **URL**       | :no_entry:          |                          |
| **Posição**   | :black_circle:      | `atendimento-faq-titulo` |
| **Cor**       | :no_entry:          |                          |

***
